;;; kak-colon.el --- colon keys for kakoune emacs -*- lexical-binding: t, -*-

;;; Version: 1.0.0
;; Keywords: kakoune, ryo
;; Package-Requires: ((emacs "27.2"))

;;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; Provides colon key support similar to EVIL/Kakoune colon keys.
;; Intended for use with 'kakoune.el', but not required.
;; Commands can be easily modified.
;; Comes with interactive completion support,
;; 'marginalia-kak-colon' is needed for marginalia support.

;;; Code:

(require 'ryo-modal)

(setq kak-colon-cmd-alist
                   '(("q" . kill-this-buffer)
                    ("w" . save-buffer)
                    ("wq" . write-quit)
                    ("was" . write-file)
                    ("B" . find/buffer)
                    ("b" . switch-to-buffer)
                    ("bn" . next-buffer)
                    ("bp" . previous-buffer)
                    ;;expanded form
                    ("write-buffer" . save-buffer)
                    ("save-quit" . write-quit)
                    ("write-as" . write-file)
                    ("kill-buffer" . kill-this-buffer)
                    ("find/buffer" . find/buffer)
                    ))

(defun kak-colon-setup ()
  "Set up colon keybinds."
  (ryo-modal-keys
    (":" kak-colon-cmd :name "colon-command")))

(defun kak-colon-cmd ()
  "Call a kak-colon command."
  (interactive)
	(let* ((choice (completing-read ":" (mark-cat-alist))))
		(call-interactively (cdr (assoc choice kak-colon-cmd-alist)))))

(defun mark-cat-alist ()
  "Mark metadata CATEGORY as alist."
  (lambda (str pred flag)
    (pcase flag
      ('metadata
       `(metadata (category . ,'alist)))
      (_
       (all-completions str kak-colon-cmd-alist pred)))))

;;COLON FUNCTIONS
(defun find/buffer ()
  "Find a buffer or switch to buffer."
  (interactive)
  (call-interactively
    (intern (completing-read "Choose one: " '(find-file switch-to-buffer)))))

;;only close buffer
(defun write-quit ()
  "Write and then quit buffer."
  (interactive)
  (save-buffer)
  (kill-this-buffer)
)

(provide 'kak-colon)
;;; kak-colon.el ends here



