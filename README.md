# Ryo Colon Commands

Colon commands using `ryo-modal` intended to be similar to Kakoune's colon commands.
Can be used alongside `kakoune-el` for a more kakoune-like experience.

![screenshot](./screenshot.jpeg)

This package uses `completing-read` to allow for interactive completion using vertico.
There is another package: `marginalia-kak-colon.el` which will allow for marginalia annotations next to the completions.

As far as I know, there aren't any colon command packages (eg. EVIL) that support vertical interactive completion as well as marginalia annotations.

The commands, as well as the annotations are easily configurable.
