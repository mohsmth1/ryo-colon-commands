;;; marginalia-kak-colon.el --- marginalia for kakoune colon keys -*- lexical-binding: t, -*-

;;; Version: 1.0.0
;; Keywords: kakoune, ryo, marginalia
;; Package-Requires: ((emacs "27.2"))

;;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; Marginalia support for kakoune colon commands

;;; Code:

(require 'marginalia)
(require 'kak-colon)

(setq marginalia-annotator-registry
  (cons
    (list 'alist 'marginalia-annotate-alist)
              marginalia-annotator-registry))

(defun marginalia-annotate-alist (cmdstr)
  "Use CMDSTR to get function CAND annotations."
    (setq cand (match-kak-cmd cmdstr))
  (when-let (sym (intern-soft cand))
    (when (fboundp sym)
      (concat ;hotkey text
       (marginalia-annotate-binding cand)
       (marginalia--fields

        ((marginalia--function-doc sym) :truncate 1.0
         :face 'marginalia-documentation))))))

(defun match-kak-cmd (cmdstr)
  "Match CMDSTR with its alist cell to get associated command name."
  (cdr (assoc cmdstr kak-colon-cmd-alist (lambda (x regexp) (let ((case-fold-search nil))(string-match-p regexp x))))))

(provide 'marginalia-kak-colon)
;;; marginalia-kak-colon.el ends here
